//
//  main.m
//  ConcreteSolutions
//
//  Created by Rafael Silva on 26/12/16.
//  Copyright © 2016 Rafael Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
