//
//  DetailViewController.m
//  ConcreteSolutions
//
//  Created by Rafael Silva on 28/12/16.
//  Copyright © 2016 Rafael Silva. All rights reserved.
//

#import "DetailViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "DCKeyValueObjectMapping.h"
#import "DCArrayMapping.h"
#import "DCParserConfiguration.h"
#import "BaseClass.h"
#import "Items.h"
#import "Owner.h"
#import "Milestone.h"
#import "User.h"
#import "Repo.h"
#import "Creator.h"
#import "Tools.h"
@interface DetailViewController ()
{
    BaseClass *base;
    NSArray *arrayList;
    NSArray *arrayJson;
    BOOL isLoading;
    NSInteger numeroPaginas;
}
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =  self.item.name;
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = 120;
    arrayList = [[NSMutableArray alloc]init];
    [self getData:self.item];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:10];
    UILabel *descLabel = (UILabel *)[cell viewWithTag:11];
    UIImageView *avatarImage = (UIImageView *)[cell viewWithTag:14];
    UILabel *nickNameLabel = (UILabel *)[cell viewWithTag:12];
    UILabel *fullNameLabel = (UILabel *)[cell viewWithTag:13];
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:15];
    
    Tools *manager = [[Tools alloc]init];
    BaseClass *obj = [arrayList objectAtIndex:indexPath.row];
    
    titleLabel.text =  obj.title;
    descLabel.text = obj.body;
    nickNameLabel.text =  obj.base.repo.name;
    fullNameLabel.text = obj.base.repo.fullName;
    [self circleImage:avatarImage];
    [avatarImage sd_setImageWithURL:[NSURL URLWithString:obj.base.repo.owner.avatarUrl] placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
    
    NSDate *date = [manager dateFromString:obj.base.repo.pushedAt withFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    [dateLabel setText: [manager dateToStringFromTodayTo:date]];//2016-12-28T21:03:45Z"

    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     BaseClass *obj = [arrayList objectAtIndex:indexPath.row];
    NSURL *url = [NSURL URLWithString:obj.url];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)circleImage:(UIImageView *)image{
    image.layer.cornerRadius = image.frame.size.width / 2;
    image.clipsToBounds = YES;
    image.layer.borderColor = [UIColor whiteColor].CGColor;
    image.layer.borderWidth = 1;
}


-(void)getData:(Items *)obj{
    self.activity.hidden = NO;
    [self.activity startAnimating];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
   
    NSString *url = [NSString stringWithFormat:@"https://api.github.com/repos/%@/%@/pulls",obj.owner.login,obj.name];
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            self.lbPlaceHolder.hidden = NO;
            self.activity.hidden = YES;
            [self.activity stopAnimating];

            NSLog(@"Error: %@", error);
        } else {
            self.lbPlaceHolder.hidden = YES;
            self.activity.hidden = YES;
            [self.activity stopAnimating];
            isLoading = false;
            
            NSLog(@"%@ %@", response, responseObject);
            DCParserConfiguration *config = [DCParserConfiguration configuration];
           
            DCObjectMapping *idOwner = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"ownerIdentifier" onClass:[Owner class]];

            DCObjectMapping *idRep = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"repoIdentifier" onClass:[Repo class]];
          
            DCObjectMapping *descMapping = [DCObjectMapping mapKeyPath:@"description" toAttribute:@"repoDescription" onClass:[Repo class]];

            DCObjectMapping *idUser = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"userIdentifier" onClass:[User class]];

            DCObjectMapping *idCreator = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"creatorIdentifier" onClass:[Creator class]];

            DCObjectMapping *idMapping = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"milestoneIdentifier" onClass:[Milestone class]];
          
            DCObjectMapping *descMil = [DCObjectMapping mapKeyPath:@"description" toAttribute:@"milestoneDescription" onClass:[Milestone class]];

            DCObjectMapping *idBaseClass = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"internalBaseClassIdentifier" onClass:[BaseClass class]];


            [config addObjectMapping:idOwner];
            [config addObjectMapping:idRep];
            [config addObjectMapping:descMapping];
            [config addObjectMapping:idUser];
            [config addObjectMapping:idCreator];
            [config addObjectMapping:idMapping];
            [config addObjectMapping:descMil];
            [config addObjectMapping:idBaseClass];
  
            DCKeyValueObjectMapping *parse = [DCKeyValueObjectMapping mapperForClass:[BaseClass class]andConfiguration:config];
            
            arrayList = [parse parseArray:responseObject];
            [self.tableView reloadData];
        }
    }];
    [dataTask resume];
}
@end
