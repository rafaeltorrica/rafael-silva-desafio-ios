//
//  GitListViewController.h
//  ConcreteSolutions
//
//  Created by Rafael Silva on 26/12/16.
//  Copyright © 2016 Rafael Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@class AFNetworking;

@interface GitListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actitity;
@property (weak, nonatomic) IBOutlet UILabel *lbPlaceHolder;

@end
