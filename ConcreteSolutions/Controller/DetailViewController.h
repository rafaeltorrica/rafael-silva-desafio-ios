//
//  DetailViewController.h
//  ConcreteSolutions
//
//  Created by Rafael Silva on 28/12/16.
//  Copyright © 2016 Rafael Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Items.h"

@interface DetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UILabel *lbPlaceHolder;

@property(nonatomic, strong) Items *item;
@end
