//
//  GitListViewController.m
//  ConcreteSolutions
//
//  Created by Rafael Silva on 26/12/16.
//  Copyright © 2016 Rafael Silva. All rights reserved.
//

#import "GitListViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DCKeyValueObjectMapping.h"
#import "DCArrayMapping.h"
#import "DCParserConfiguration.h"
#import "BaseClass.h"
#import "Items.h"
#import "Owner.h"
#import "DetailViewController.h"
@interface GitListViewController ()
{
    BaseClass *base;
    NSMutableArray *arrayList;
    NSArray *arrayJson;
    BOOL isLoading;
    NSInteger numeroPaginas;
}
@end

@implementation GitListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =  @"Gite Teste";

    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = 100;

    arrayList = [[NSMutableArray alloc]init];
    [self getData:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
   
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *descLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *likeLabel = (UILabel *)[cell viewWithTag:3];
    UILabel *countLabel = (UILabel *)[cell viewWithTag:4];
    UIImageView *avatarImage = (UIImageView *)[cell viewWithTag:5];
    UILabel *nickNameLabel = (UILabel *)[cell viewWithTag:6];
    UILabel *fullNameLabel = (UILabel *)[cell viewWithTag:7];
    
    Items *obj = [arrayList objectAtIndex:indexPath.row];
    
    titleLabel.text =  obj.name;
    descLabel.text = obj.itemsDescription;
    nickNameLabel.text =  obj.name;
    fullNameLabel.text = obj.fullName;
    countLabel.text = [NSString stringWithFormat:@"%.0f",obj.forksCount];
    likeLabel.text = [NSString stringWithFormat:@"%.0f",obj.stargazersCount];

    [self circleImage:avatarImage];
    [avatarImage sd_setImageWithURL:[NSURL URLWithString:obj.owner.avatarUrl] placeholderImage:[UIImage imageNamed:@"user_placeholder"]];

    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"segueDetail" sender:self];
}
-(void)circleImage:(UIImageView *)image{
    //circle
    
    image.layer.cornerRadius = image.frame.size.width / 2;
    image.clipsToBounds = YES;
    image.layer.borderColor = [UIColor whiteColor].CGColor;
    image.layer.borderWidth = 1;
}

#pragma mark - AFNetworking
-(void)getData:(NSInteger )page{
    self.actitity.hidden = NO;
    [self.actitity startAnimating];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSString *url = [NSString stringWithFormat:@"http://api.github.com/search/repositories?q=language:Java&sort=stars&page=%d",page];
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            self.lbPlaceHolder.hidden = NO;
            self.actitity.hidden = YES;
            [self.actitity stopAnimating];

            NSLog(@"Error: %@", error);
        } else {
            self.lbPlaceHolder.hidden = YES;
            self.actitity.hidden = YES;
            [self.actitity stopAnimating];
            isLoading = false;

            NSLog(@"%@ %@", response, responseObject);
            DCParserConfiguration *config = [DCParserConfiguration configuration];
            
            DCObjectMapping *idMapping = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"ownerIdentifier" onClass:[Owner class]];
          
            DCObjectMapping *idItemMapping = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"itemsIdentifier" onClass:[Items class]];
           
            DCObjectMapping *descMapping = [DCObjectMapping mapKeyPath:@"description" toAttribute:@"itemsDescription" onClass:[Items class]];
            //
            
            DCArrayMapping *itens = [DCArrayMapping mapperForClassElements:[Items class] forAttribute:@"items" onClass:[BaseClass class]];
            
            [config addObjectMapping:idMapping];
            [config addObjectMapping:idItemMapping];
            [config addObjectMapping:descMapping];
            [config addArrayMapper:itens];

            DCKeyValueObjectMapping *parse = [DCKeyValueObjectMapping mapperForClass:[BaseClass class]andConfiguration:config];
            
            base = [parse parseDictionary:responseObject];
            
            arrayJson = [NSArray arrayWithArray:base.items];
            [arrayList addObjectsFromArray:arrayJson];
            [self.tableView reloadData];
        }
    }];
    [dataTask resume];
}
#pragma mark - ScrollView
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.tableView){
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height -200)
        {
            if (!isLoading) {
                if (numeroPaginas != base.items.count) {
                    
                    isLoading = true;
                    
                    numeroPaginas++;
               
                    [self getData:numeroPaginas];
                    
                }
            }
        }
    }
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueDetail"]) {
        DetailViewController *detail = [segue destinationViewController];
        Items *Teste = arrayList[self.tableView.indexPathForSelectedRow.row];
        detail.item = Teste;

        
    }
}


@end
