//
//  Tools.m
//  ConcreteSolutions
//
//  Created by Rafael Silva on 28/12/16.
//  Copyright © 2016 Rafael Silva. All rights reserved.
//

#import "Tools.h"

@implementation Tools
static Tools* sharedInstance = nil;

+(instancetype) sharedInstance
{
    @synchronized(self)
    {
        if (sharedInstance == nil)
        {
            sharedInstance = [[Tools alloc] init];
        }
    }
    
    return sharedInstance;
}

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}
-(NSDate *)dateFromString: (NSString *)stringDate {
    return [self dateFromString:stringDate withFormat:@"yyyy-MM-dd"];
}

-(NSDate *)dateFromString: (NSString *)stringDate withFormat:(NSString *) format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSDate *dateFormatted;
    
    [dateFormatter setDateFormat:format];
    [dateFormatter setCalendar:[NSCalendar calendarWithIdentifier:NSCalendarIdentifierISO8601]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    dateFormatted = [dateFormatter dateFromString:stringDate];
    
    return dateFormatted;
}
-(NSString *) dateToStringFromTodayTo: (NSDate*) baseDate
{
    //Today
    NSDate *today = [NSDate date];
    
    NSCalendar *myCalendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    NSDateComponents *currentComps = [myCalendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate: today];
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    
    //If the baseDate IS today
    if ([myCalendar isDateInToday: baseDate])
    {
        myDateFormatter.dateFormat = @"HH:mm";
        [myDateFormatter setCalendar:[NSCalendar calendarWithIdentifier:NSCalendarIdentifierISO8601]];
        [myDateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
        [myDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSString *teste = [myDateFormatter stringFromDate: baseDate];
        return teste;
    }
    //If the baseDate IS yerterday
    else if ([myCalendar isDateInYesterday: baseDate])
    {
        return @"ontem";
    }
    
    //Get the first day of this week
    [currentComps setWeekday:1]; // 1: Domingo
    NSDate *firstDayOfTheWeek = [myCalendar dateFromComponents: currentComps];
    
    //If the baseDate IS between the first day of this week AND today
    if ([self isDate: baseDate inRangeFirstDate: firstDayOfTheWeek lastDate: today])
    {
        myDateFormatter.dateFormat = @"EEEE";
        return [myDateFormatter stringFromDate: baseDate];
    }
    
    //If the baseDate is BEFORE this week OR after today
    myDateFormatter.dateFormat = @"dd/MM/yy";
    return [myDateFormatter stringFromDate: baseDate];
}

-(BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate
{
    return [date compare:firstDate] == NSOrderedDescending &&
    [date compare:lastDate]  == NSOrderedAscending;
}
@end
