//
//  Tools.h
//  ConcreteSolutions
//
//  Created by Rafael Silva on 28/12/16.
//  Copyright © 2016 Rafael Silva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tools : NSObject
+(instancetype) sharedInstance;
-(NSDate *)dateFromString: (NSString *)stringDate withFormat:(NSString *) format;
-(NSString *) dateToStringFromTodayTo: (NSDate*) baseDate;

@end
