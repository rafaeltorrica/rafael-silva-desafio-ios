//
//  Milestone.h
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Creator;

@interface Milestone : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double milestoneIdentifier;
@property (nonatomic, strong) NSString *milestoneDescription;
@property (nonatomic, assign) double openIssues;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *labelsUrl;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, assign) double closedIssues;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) Creator *creator;
@property (nonatomic, strong) NSString *htmlUrl;
@property (nonatomic, assign) double number;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, assign) id dueOn;
@property (nonatomic, assign) id closedAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
