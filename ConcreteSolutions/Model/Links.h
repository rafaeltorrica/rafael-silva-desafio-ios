//
//  Links.h
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Comments, Issue, Commits, Html, Statuses, SelfClass, ReviewComment, ReviewComments;

@interface Links : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) Comments *comments;
@property (nonatomic, strong) Issue *issue;
@property (nonatomic, strong) Commits *commits;
@property (nonatomic, strong) Html *html;
@property (nonatomic, strong) Statuses *statuses;
@property (nonatomic, strong) SelfClass *linksSelf;
@property (nonatomic, strong) ReviewComment *reviewComment;
@property (nonatomic, strong) ReviewComments *reviewComments;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
