//
//  Head.m
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Head.h"
#import "Repo.h"
#import "User.h"


NSString *const kHeadRef = @"ref";
NSString *const kHeadLabel = @"label";
NSString *const kHeadSha = @"sha";
NSString *const kHeadRepo = @"repo";
NSString *const kHeadUser = @"user";


@interface Head ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Head

@synthesize ref = _ref;
@synthesize label = _label;
@synthesize sha = _sha;
@synthesize repo = _repo;
@synthesize user = _user;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ref = [self objectOrNilForKey:kHeadRef fromDictionary:dict];
            self.label = [self objectOrNilForKey:kHeadLabel fromDictionary:dict];
            self.sha = [self objectOrNilForKey:kHeadSha fromDictionary:dict];
            self.repo = [Repo modelObjectWithDictionary:[dict objectForKey:kHeadRepo]];
            self.user = [User modelObjectWithDictionary:[dict objectForKey:kHeadUser]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.ref forKey:kHeadRef];
    [mutableDict setValue:self.label forKey:kHeadLabel];
    [mutableDict setValue:self.sha forKey:kHeadSha];
    [mutableDict setValue:[self.repo dictionaryRepresentation] forKey:kHeadRepo];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kHeadUser];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.ref = [aDecoder decodeObjectForKey:kHeadRef];
    self.label = [aDecoder decodeObjectForKey:kHeadLabel];
    self.sha = [aDecoder decodeObjectForKey:kHeadSha];
    self.repo = [aDecoder decodeObjectForKey:kHeadRepo];
    self.user = [aDecoder decodeObjectForKey:kHeadUser];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_ref forKey:kHeadRef];
    [aCoder encodeObject:_label forKey:kHeadLabel];
    [aCoder encodeObject:_sha forKey:kHeadSha];
    [aCoder encodeObject:_repo forKey:kHeadRepo];
    [aCoder encodeObject:_user forKey:kHeadUser];
}

- (id)copyWithZone:(NSZone *)zone
{
    Head *copy = [[Head alloc] init];
    
    if (copy) {

        copy.ref = [self.ref copyWithZone:zone];
        copy.label = [self.label copyWithZone:zone];
        copy.sha = [self.sha copyWithZone:zone];
        copy.repo = [self.repo copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
    }
    
    return copy;
}


@end
