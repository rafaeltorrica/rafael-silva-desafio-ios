//
//  BaseClass.h
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Base.h"
#import "Head.h"
#import "Links.h"
@class Base, User, Head, Links, Milestone;

@interface BaseClass : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double totalCount;
@property (nonatomic, assign) BOOL incompleteResults;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) Milestone *milestone;
@property (nonatomic, assign) BOOL locked;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *commitsUrl;
@property (nonatomic, strong) NSString *mergeCommitSha;
@property (nonatomic, strong) NSString *reviewCommentUrl;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) Base *base;
@property (nonatomic, strong) NSString *reviewCommentsUrl;
@property (nonatomic, strong) NSString *assignee;
@property (nonatomic, strong) NSString *commentsUrl;
@property (nonatomic, strong) NSString *patchUrl;
@property (nonatomic, strong) NSString *mergedAt;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, assign) double number;
@property (nonatomic, strong) NSString *issueUrl;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSString *closedAt;
@property (nonatomic, strong) Head *head;
@property (nonatomic, strong) NSArray *assignees;
@property (nonatomic, strong) NSString *statusesUrl;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) Links *links;
@property (nonatomic, strong) NSString *diffUrl;
@property (nonatomic, strong) NSString *htmlUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
