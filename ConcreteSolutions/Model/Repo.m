//
//  Repo.m
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Repo.h"
#import "Owner.h"


NSString *const kRepoKeysUrl = @"keys_url";
NSString *const kRepoStatusesUrl = @"statuses_url";
NSString *const kRepoIssuesUrl = @"issues_url";
NSString *const kRepoLanguage = @"language";
NSString *const kRepoIssueEventsUrl = @"issue_events_url";
NSString *const kRepoId = @"id";
NSString *const kRepoOwner = @"owner";
NSString *const kRepoEventsUrl = @"events_url";
NSString *const kRepoSubscriptionUrl = @"subscription_url";
NSString *const kRepoWatchers = @"watchers";
NSString *const kRepoGitCommitsUrl = @"git_commits_url";
NSString *const kRepoSubscribersUrl = @"subscribers_url";
NSString *const kRepoCloneUrl = @"clone_url";
NSString *const kRepoHasWiki = @"has_wiki";
NSString *const kRepoPullsUrl = @"pulls_url";
NSString *const kRepoUrl = @"url";
NSString *const kRepoFork = @"fork";
NSString *const kRepoNotificationsUrl = @"notifications_url";
NSString *const kRepoDescription = @"description";
NSString *const kRepoCollaboratorsUrl = @"collaborators_url";
NSString *const kRepoDeploymentsUrl = @"deployments_url";
NSString *const kRepoLanguagesUrl = @"languages_url";
NSString *const kRepoHasIssues = @"has_issues";
NSString *const kRepoCommentsUrl = @"comments_url";
NSString *const kRepoPrivate = @"private";
NSString *const kRepoSize = @"size";
NSString *const kRepoGitTagsUrl = @"git_tags_url";
NSString *const kRepoUpdatedAt = @"updated_at";
NSString *const kRepoSshUrl = @"ssh_url";
NSString *const kRepoName = @"name";
NSString *const kRepoArchiveUrl = @"archive_url";
NSString *const kRepoOpenIssuesCount = @"open_issues_count";
NSString *const kRepoMilestonesUrl = @"milestones_url";
NSString *const kRepoBlobsUrl = @"blobs_url";
NSString *const kRepoContributorsUrl = @"contributors_url";
NSString *const kRepoContentsUrl = @"contents_url";
NSString *const kRepoForksCount = @"forks_count";
NSString *const kRepoTreesUrl = @"trees_url";
NSString *const kRepoMirrorUrl = @"mirror_url";
NSString *const kRepoHasDownloads = @"has_downloads";
NSString *const kRepoCreatedAt = @"created_at";
NSString *const kRepoForksUrl = @"forks_url";
NSString *const kRepoSvnUrl = @"svn_url";
NSString *const kRepoCommitsUrl = @"commits_url";
NSString *const kRepoHomepage = @"homepage";
NSString *const kRepoTeamsUrl = @"teams_url";
NSString *const kRepoBranchesUrl = @"branches_url";
NSString *const kRepoIssueCommentUrl = @"issue_comment_url";
NSString *const kRepoMergesUrl = @"merges_url";
NSString *const kRepoGitRefsUrl = @"git_refs_url";
NSString *const kRepoGitUrl = @"git_url";
NSString *const kRepoForks = @"forks";
NSString *const kRepoOpenIssues = @"open_issues";
NSString *const kRepoHooksUrl = @"hooks_url";
NSString *const kRepoHtmlUrl = @"html_url";
NSString *const kRepoStargazersUrl = @"stargazers_url";
NSString *const kRepoHasPages = @"has_pages";
NSString *const kRepoAssigneesUrl = @"assignees_url";
NSString *const kRepoCompareUrl = @"compare_url";
NSString *const kRepoFullName = @"full_name";
NSString *const kRepoTagsUrl = @"tags_url";
NSString *const kRepoReleasesUrl = @"releases_url";
NSString *const kRepoPushedAt = @"pushed_at";
NSString *const kRepoLabelsUrl = @"labels_url";
NSString *const kRepoDownloadsUrl = @"downloads_url";
NSString *const kRepoDefaultBranch = @"default_branch";
NSString *const kRepoStargazersCount = @"stargazers_count";
NSString *const kRepoWatchersCount = @"watchers_count";


@interface Repo ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Repo

@synthesize keysUrl = _keysUrl;
@synthesize statusesUrl = _statusesUrl;
@synthesize issuesUrl = _issuesUrl;
@synthesize language = _language;
@synthesize issueEventsUrl = _issueEventsUrl;
@synthesize repoIdentifier = _repoIdentifier;
@synthesize owner = _owner;
@synthesize eventsUrl = _eventsUrl;
@synthesize subscriptionUrl = _subscriptionUrl;
@synthesize watchers = _watchers;
@synthesize gitCommitsUrl = _gitCommitsUrl;
@synthesize subscribersUrl = _subscribersUrl;
@synthesize cloneUrl = _cloneUrl;
@synthesize hasWiki = _hasWiki;
@synthesize pullsUrl = _pullsUrl;
@synthesize url = _url;
@synthesize fork = _fork;
@synthesize notificationsUrl = _notificationsUrl;
@synthesize repoDescription = _repoDescription;
@synthesize collaboratorsUrl = _collaboratorsUrl;
@synthesize deploymentsUrl = _deploymentsUrl;
@synthesize languagesUrl = _languagesUrl;
@synthesize hasIssues = _hasIssues;
@synthesize commentsUrl = _commentsUrl;
@synthesize privateProperty = _privateProperty;
@synthesize size = _size;
@synthesize gitTagsUrl = _gitTagsUrl;
@synthesize updatedAt = _updatedAt;
@synthesize sshUrl = _sshUrl;
@synthesize name = _name;
@synthesize archiveUrl = _archiveUrl;
@synthesize openIssuesCount = _openIssuesCount;
@synthesize milestonesUrl = _milestonesUrl;
@synthesize blobsUrl = _blobsUrl;
@synthesize contributorsUrl = _contributorsUrl;
@synthesize contentsUrl = _contentsUrl;
@synthesize forksCount = _forksCount;
@synthesize treesUrl = _treesUrl;
@synthesize mirrorUrl = _mirrorUrl;
@synthesize hasDownloads = _hasDownloads;
@synthesize createdAt = _createdAt;
@synthesize forksUrl = _forksUrl;
@synthesize svnUrl = _svnUrl;
@synthesize commitsUrl = _commitsUrl;
@synthesize homepage = _homepage;
@synthesize teamsUrl = _teamsUrl;
@synthesize branchesUrl = _branchesUrl;
@synthesize issueCommentUrl = _issueCommentUrl;
@synthesize mergesUrl = _mergesUrl;
@synthesize gitRefsUrl = _gitRefsUrl;
@synthesize gitUrl = _gitUrl;
@synthesize forks = _forks;
@synthesize openIssues = _openIssues;
@synthesize hooksUrl = _hooksUrl;
@synthesize htmlUrl = _htmlUrl;
@synthesize stargazersUrl = _stargazersUrl;
@synthesize hasPages = _hasPages;
@synthesize assigneesUrl = _assigneesUrl;
@synthesize compareUrl = _compareUrl;
@synthesize fullName = _fullName;
@synthesize tagsUrl = _tagsUrl;
@synthesize releasesUrl = _releasesUrl;
@synthesize pushedAt = _pushedAt;
@synthesize labelsUrl = _labelsUrl;
@synthesize downloadsUrl = _downloadsUrl;
@synthesize defaultBranch = _defaultBranch;
@synthesize stargazersCount = _stargazersCount;
@synthesize watchersCount = _watchersCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.keysUrl = [self objectOrNilForKey:kRepoKeysUrl fromDictionary:dict];
            self.statusesUrl = [self objectOrNilForKey:kRepoStatusesUrl fromDictionary:dict];
            self.issuesUrl = [self objectOrNilForKey:kRepoIssuesUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kRepoLanguage fromDictionary:dict];
            self.issueEventsUrl = [self objectOrNilForKey:kRepoIssueEventsUrl fromDictionary:dict];
            self.repoIdentifier = [[self objectOrNilForKey:kRepoId fromDictionary:dict] doubleValue];
            self.owner = [Owner modelObjectWithDictionary:[dict objectForKey:kRepoOwner]];
            self.eventsUrl = [self objectOrNilForKey:kRepoEventsUrl fromDictionary:dict];
            self.subscriptionUrl = [self objectOrNilForKey:kRepoSubscriptionUrl fromDictionary:dict];
            self.watchers = [[self objectOrNilForKey:kRepoWatchers fromDictionary:dict] doubleValue];
            self.gitCommitsUrl = [self objectOrNilForKey:kRepoGitCommitsUrl fromDictionary:dict];
            self.subscribersUrl = [self objectOrNilForKey:kRepoSubscribersUrl fromDictionary:dict];
            self.cloneUrl = [self objectOrNilForKey:kRepoCloneUrl fromDictionary:dict];
            self.hasWiki = [[self objectOrNilForKey:kRepoHasWiki fromDictionary:dict] boolValue];
            self.pullsUrl = [self objectOrNilForKey:kRepoPullsUrl fromDictionary:dict];
            self.url = [self objectOrNilForKey:kRepoUrl fromDictionary:dict];
            self.fork = [[self objectOrNilForKey:kRepoFork fromDictionary:dict] boolValue];
            self.notificationsUrl = [self objectOrNilForKey:kRepoNotificationsUrl fromDictionary:dict];
            self.repoDescription = [self objectOrNilForKey:kRepoDescription fromDictionary:dict];
            self.collaboratorsUrl = [self objectOrNilForKey:kRepoCollaboratorsUrl fromDictionary:dict];
            self.deploymentsUrl = [self objectOrNilForKey:kRepoDeploymentsUrl fromDictionary:dict];
            self.languagesUrl = [self objectOrNilForKey:kRepoLanguagesUrl fromDictionary:dict];
            self.hasIssues = [[self objectOrNilForKey:kRepoHasIssues fromDictionary:dict] boolValue];
            self.commentsUrl = [self objectOrNilForKey:kRepoCommentsUrl fromDictionary:dict];
            self.privateProperty = [[self objectOrNilForKey:kRepoPrivate fromDictionary:dict] boolValue];
            self.size = [[self objectOrNilForKey:kRepoSize fromDictionary:dict] doubleValue];
            self.gitTagsUrl = [self objectOrNilForKey:kRepoGitTagsUrl fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kRepoUpdatedAt fromDictionary:dict];
            self.sshUrl = [self objectOrNilForKey:kRepoSshUrl fromDictionary:dict];
            self.name = [self objectOrNilForKey:kRepoName fromDictionary:dict];
            self.archiveUrl = [self objectOrNilForKey:kRepoArchiveUrl fromDictionary:dict];
            self.openIssuesCount = [[self objectOrNilForKey:kRepoOpenIssuesCount fromDictionary:dict] doubleValue];
            self.milestonesUrl = [self objectOrNilForKey:kRepoMilestonesUrl fromDictionary:dict];
            self.blobsUrl = [self objectOrNilForKey:kRepoBlobsUrl fromDictionary:dict];
            self.contributorsUrl = [self objectOrNilForKey:kRepoContributorsUrl fromDictionary:dict];
            self.contentsUrl = [self objectOrNilForKey:kRepoContentsUrl fromDictionary:dict];
            self.forksCount = [[self objectOrNilForKey:kRepoForksCount fromDictionary:dict] doubleValue];
            self.treesUrl = [self objectOrNilForKey:kRepoTreesUrl fromDictionary:dict];
            self.mirrorUrl = [self objectOrNilForKey:kRepoMirrorUrl fromDictionary:dict];
            self.hasDownloads = [[self objectOrNilForKey:kRepoHasDownloads fromDictionary:dict] boolValue];
            self.createdAt = [self objectOrNilForKey:kRepoCreatedAt fromDictionary:dict];
            self.forksUrl = [self objectOrNilForKey:kRepoForksUrl fromDictionary:dict];
            self.svnUrl = [self objectOrNilForKey:kRepoSvnUrl fromDictionary:dict];
            self.commitsUrl = [self objectOrNilForKey:kRepoCommitsUrl fromDictionary:dict];
            self.homepage = [self objectOrNilForKey:kRepoHomepage fromDictionary:dict];
            self.teamsUrl = [self objectOrNilForKey:kRepoTeamsUrl fromDictionary:dict];
            self.branchesUrl = [self objectOrNilForKey:kRepoBranchesUrl fromDictionary:dict];
            self.issueCommentUrl = [self objectOrNilForKey:kRepoIssueCommentUrl fromDictionary:dict];
            self.mergesUrl = [self objectOrNilForKey:kRepoMergesUrl fromDictionary:dict];
            self.gitRefsUrl = [self objectOrNilForKey:kRepoGitRefsUrl fromDictionary:dict];
            self.gitUrl = [self objectOrNilForKey:kRepoGitUrl fromDictionary:dict];
            self.forks = [[self objectOrNilForKey:kRepoForks fromDictionary:dict] doubleValue];
            self.openIssues = [[self objectOrNilForKey:kRepoOpenIssues fromDictionary:dict] doubleValue];
            self.hooksUrl = [self objectOrNilForKey:kRepoHooksUrl fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kRepoHtmlUrl fromDictionary:dict];
            self.stargazersUrl = [self objectOrNilForKey:kRepoStargazersUrl fromDictionary:dict];
            self.hasPages = [[self objectOrNilForKey:kRepoHasPages fromDictionary:dict] boolValue];
            self.assigneesUrl = [self objectOrNilForKey:kRepoAssigneesUrl fromDictionary:dict];
            self.compareUrl = [self objectOrNilForKey:kRepoCompareUrl fromDictionary:dict];
            self.fullName = [self objectOrNilForKey:kRepoFullName fromDictionary:dict];
            self.tagsUrl = [self objectOrNilForKey:kRepoTagsUrl fromDictionary:dict];
            self.releasesUrl = [self objectOrNilForKey:kRepoReleasesUrl fromDictionary:dict];
            self.pushedAt = [self objectOrNilForKey:kRepoPushedAt fromDictionary:dict];
            self.labelsUrl = [self objectOrNilForKey:kRepoLabelsUrl fromDictionary:dict];
            self.downloadsUrl = [self objectOrNilForKey:kRepoDownloadsUrl fromDictionary:dict];
            self.defaultBranch = [self objectOrNilForKey:kRepoDefaultBranch fromDictionary:dict];
            self.stargazersCount = [[self objectOrNilForKey:kRepoStargazersCount fromDictionary:dict] doubleValue];
            self.watchersCount = [[self objectOrNilForKey:kRepoWatchersCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.keysUrl forKey:kRepoKeysUrl];
    [mutableDict setValue:self.statusesUrl forKey:kRepoStatusesUrl];
    [mutableDict setValue:self.issuesUrl forKey:kRepoIssuesUrl];
    [mutableDict setValue:self.language forKey:kRepoLanguage];
    [mutableDict setValue:self.issueEventsUrl forKey:kRepoIssueEventsUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.repoIdentifier] forKey:kRepoId];
    [mutableDict setValue:[self.owner dictionaryRepresentation] forKey:kRepoOwner];
    [mutableDict setValue:self.eventsUrl forKey:kRepoEventsUrl];
    [mutableDict setValue:self.subscriptionUrl forKey:kRepoSubscriptionUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.watchers] forKey:kRepoWatchers];
    [mutableDict setValue:self.gitCommitsUrl forKey:kRepoGitCommitsUrl];
    [mutableDict setValue:self.subscribersUrl forKey:kRepoSubscribersUrl];
    [mutableDict setValue:self.cloneUrl forKey:kRepoCloneUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasWiki] forKey:kRepoHasWiki];
    [mutableDict setValue:self.pullsUrl forKey:kRepoPullsUrl];
    [mutableDict setValue:self.url forKey:kRepoUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.fork] forKey:kRepoFork];
    [mutableDict setValue:self.notificationsUrl forKey:kRepoNotificationsUrl];
    [mutableDict setValue:self.repoDescription forKey:kRepoDescription];
    [mutableDict setValue:self.collaboratorsUrl forKey:kRepoCollaboratorsUrl];
    [mutableDict setValue:self.deploymentsUrl forKey:kRepoDeploymentsUrl];
    [mutableDict setValue:self.languagesUrl forKey:kRepoLanguagesUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasIssues] forKey:kRepoHasIssues];
    [mutableDict setValue:self.commentsUrl forKey:kRepoCommentsUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.privateProperty] forKey:kRepoPrivate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kRepoSize];
    [mutableDict setValue:self.gitTagsUrl forKey:kRepoGitTagsUrl];
    [mutableDict setValue:self.updatedAt forKey:kRepoUpdatedAt];
    [mutableDict setValue:self.sshUrl forKey:kRepoSshUrl];
    [mutableDict setValue:self.name forKey:kRepoName];
    [mutableDict setValue:self.archiveUrl forKey:kRepoArchiveUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.openIssuesCount] forKey:kRepoOpenIssuesCount];
    [mutableDict setValue:self.milestonesUrl forKey:kRepoMilestonesUrl];
    [mutableDict setValue:self.blobsUrl forKey:kRepoBlobsUrl];
    [mutableDict setValue:self.contributorsUrl forKey:kRepoContributorsUrl];
    [mutableDict setValue:self.contentsUrl forKey:kRepoContentsUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.forksCount] forKey:kRepoForksCount];
    [mutableDict setValue:self.treesUrl forKey:kRepoTreesUrl];
    [mutableDict setValue:self.mirrorUrl forKey:kRepoMirrorUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasDownloads] forKey:kRepoHasDownloads];
    [mutableDict setValue:self.createdAt forKey:kRepoCreatedAt];
    [mutableDict setValue:self.forksUrl forKey:kRepoForksUrl];
    [mutableDict setValue:self.svnUrl forKey:kRepoSvnUrl];
    [mutableDict setValue:self.commitsUrl forKey:kRepoCommitsUrl];
    [mutableDict setValue:self.homepage forKey:kRepoHomepage];
    [mutableDict setValue:self.teamsUrl forKey:kRepoTeamsUrl];
    [mutableDict setValue:self.branchesUrl forKey:kRepoBranchesUrl];
    [mutableDict setValue:self.issueCommentUrl forKey:kRepoIssueCommentUrl];
    [mutableDict setValue:self.mergesUrl forKey:kRepoMergesUrl];
    [mutableDict setValue:self.gitRefsUrl forKey:kRepoGitRefsUrl];
    [mutableDict setValue:self.gitUrl forKey:kRepoGitUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.forks] forKey:kRepoForks];
    [mutableDict setValue:[NSNumber numberWithDouble:self.openIssues] forKey:kRepoOpenIssues];
    [mutableDict setValue:self.hooksUrl forKey:kRepoHooksUrl];
    [mutableDict setValue:self.htmlUrl forKey:kRepoHtmlUrl];
    [mutableDict setValue:self.stargazersUrl forKey:kRepoStargazersUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasPages] forKey:kRepoHasPages];
    [mutableDict setValue:self.assigneesUrl forKey:kRepoAssigneesUrl];
    [mutableDict setValue:self.compareUrl forKey:kRepoCompareUrl];
    [mutableDict setValue:self.fullName forKey:kRepoFullName];
    [mutableDict setValue:self.tagsUrl forKey:kRepoTagsUrl];
    [mutableDict setValue:self.releasesUrl forKey:kRepoReleasesUrl];
    [mutableDict setValue:self.pushedAt forKey:kRepoPushedAt];
    [mutableDict setValue:self.labelsUrl forKey:kRepoLabelsUrl];
    [mutableDict setValue:self.downloadsUrl forKey:kRepoDownloadsUrl];
    [mutableDict setValue:self.defaultBranch forKey:kRepoDefaultBranch];
    [mutableDict setValue:[NSNumber numberWithDouble:self.stargazersCount] forKey:kRepoStargazersCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.watchersCount] forKey:kRepoWatchersCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.keysUrl = [aDecoder decodeObjectForKey:kRepoKeysUrl];
    self.statusesUrl = [aDecoder decodeObjectForKey:kRepoStatusesUrl];
    self.issuesUrl = [aDecoder decodeObjectForKey:kRepoIssuesUrl];
    self.language = [aDecoder decodeObjectForKey:kRepoLanguage];
    self.issueEventsUrl = [aDecoder decodeObjectForKey:kRepoIssueEventsUrl];
    self.repoIdentifier = [aDecoder decodeDoubleForKey:kRepoId];
    self.owner = [aDecoder decodeObjectForKey:kRepoOwner];
    self.eventsUrl = [aDecoder decodeObjectForKey:kRepoEventsUrl];
    self.subscriptionUrl = [aDecoder decodeObjectForKey:kRepoSubscriptionUrl];
    self.watchers = [aDecoder decodeDoubleForKey:kRepoWatchers];
    self.gitCommitsUrl = [aDecoder decodeObjectForKey:kRepoGitCommitsUrl];
    self.subscribersUrl = [aDecoder decodeObjectForKey:kRepoSubscribersUrl];
    self.cloneUrl = [aDecoder decodeObjectForKey:kRepoCloneUrl];
    self.hasWiki = [aDecoder decodeBoolForKey:kRepoHasWiki];
    self.pullsUrl = [aDecoder decodeObjectForKey:kRepoPullsUrl];
    self.url = [aDecoder decodeObjectForKey:kRepoUrl];
    self.fork = [aDecoder decodeBoolForKey:kRepoFork];
    self.notificationsUrl = [aDecoder decodeObjectForKey:kRepoNotificationsUrl];
    self.repoDescription = [aDecoder decodeObjectForKey:kRepoDescription];
    self.collaboratorsUrl = [aDecoder decodeObjectForKey:kRepoCollaboratorsUrl];
    self.deploymentsUrl = [aDecoder decodeObjectForKey:kRepoDeploymentsUrl];
    self.languagesUrl = [aDecoder decodeObjectForKey:kRepoLanguagesUrl];
    self.hasIssues = [aDecoder decodeBoolForKey:kRepoHasIssues];
    self.commentsUrl = [aDecoder decodeObjectForKey:kRepoCommentsUrl];
    self.privateProperty = [aDecoder decodeBoolForKey:kRepoPrivate];
    self.size = [aDecoder decodeDoubleForKey:kRepoSize];
    self.gitTagsUrl = [aDecoder decodeObjectForKey:kRepoGitTagsUrl];
    self.updatedAt = [aDecoder decodeObjectForKey:kRepoUpdatedAt];
    self.sshUrl = [aDecoder decodeObjectForKey:kRepoSshUrl];
    self.name = [aDecoder decodeObjectForKey:kRepoName];
    self.archiveUrl = [aDecoder decodeObjectForKey:kRepoArchiveUrl];
    self.openIssuesCount = [aDecoder decodeDoubleForKey:kRepoOpenIssuesCount];
    self.milestonesUrl = [aDecoder decodeObjectForKey:kRepoMilestonesUrl];
    self.blobsUrl = [aDecoder decodeObjectForKey:kRepoBlobsUrl];
    self.contributorsUrl = [aDecoder decodeObjectForKey:kRepoContributorsUrl];
    self.contentsUrl = [aDecoder decodeObjectForKey:kRepoContentsUrl];
    self.forksCount = [aDecoder decodeDoubleForKey:kRepoForksCount];
    self.treesUrl = [aDecoder decodeObjectForKey:kRepoTreesUrl];
    self.mirrorUrl = [aDecoder decodeObjectForKey:kRepoMirrorUrl];
    self.hasDownloads = [aDecoder decodeBoolForKey:kRepoHasDownloads];
    self.createdAt = [aDecoder decodeObjectForKey:kRepoCreatedAt];
    self.forksUrl = [aDecoder decodeObjectForKey:kRepoForksUrl];
    self.svnUrl = [aDecoder decodeObjectForKey:kRepoSvnUrl];
    self.commitsUrl = [aDecoder decodeObjectForKey:kRepoCommitsUrl];
    self.homepage = [aDecoder decodeObjectForKey:kRepoHomepage];
    self.teamsUrl = [aDecoder decodeObjectForKey:kRepoTeamsUrl];
    self.branchesUrl = [aDecoder decodeObjectForKey:kRepoBranchesUrl];
    self.issueCommentUrl = [aDecoder decodeObjectForKey:kRepoIssueCommentUrl];
    self.mergesUrl = [aDecoder decodeObjectForKey:kRepoMergesUrl];
    self.gitRefsUrl = [aDecoder decodeObjectForKey:kRepoGitRefsUrl];
    self.gitUrl = [aDecoder decodeObjectForKey:kRepoGitUrl];
    self.forks = [aDecoder decodeDoubleForKey:kRepoForks];
    self.openIssues = [aDecoder decodeDoubleForKey:kRepoOpenIssues];
    self.hooksUrl = [aDecoder decodeObjectForKey:kRepoHooksUrl];
    self.htmlUrl = [aDecoder decodeObjectForKey:kRepoHtmlUrl];
    self.stargazersUrl = [aDecoder decodeObjectForKey:kRepoStargazersUrl];
    self.hasPages = [aDecoder decodeBoolForKey:kRepoHasPages];
    self.assigneesUrl = [aDecoder decodeObjectForKey:kRepoAssigneesUrl];
    self.compareUrl = [aDecoder decodeObjectForKey:kRepoCompareUrl];
    self.fullName = [aDecoder decodeObjectForKey:kRepoFullName];
    self.tagsUrl = [aDecoder decodeObjectForKey:kRepoTagsUrl];
    self.releasesUrl = [aDecoder decodeObjectForKey:kRepoReleasesUrl];
    self.pushedAt = [aDecoder decodeObjectForKey:kRepoPushedAt];
    self.labelsUrl = [aDecoder decodeObjectForKey:kRepoLabelsUrl];
    self.downloadsUrl = [aDecoder decodeObjectForKey:kRepoDownloadsUrl];
    self.defaultBranch = [aDecoder decodeObjectForKey:kRepoDefaultBranch];
    self.stargazersCount = [aDecoder decodeDoubleForKey:kRepoStargazersCount];
    self.watchersCount = [aDecoder decodeDoubleForKey:kRepoWatchersCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_keysUrl forKey:kRepoKeysUrl];
    [aCoder encodeObject:_statusesUrl forKey:kRepoStatusesUrl];
    [aCoder encodeObject:_issuesUrl forKey:kRepoIssuesUrl];
    [aCoder encodeObject:_language forKey:kRepoLanguage];
    [aCoder encodeObject:_issueEventsUrl forKey:kRepoIssueEventsUrl];
    [aCoder encodeDouble:_repoIdentifier forKey:kRepoId];
    [aCoder encodeObject:_owner forKey:kRepoOwner];
    [aCoder encodeObject:_eventsUrl forKey:kRepoEventsUrl];
    [aCoder encodeObject:_subscriptionUrl forKey:kRepoSubscriptionUrl];
    [aCoder encodeDouble:_watchers forKey:kRepoWatchers];
    [aCoder encodeObject:_gitCommitsUrl forKey:kRepoGitCommitsUrl];
    [aCoder encodeObject:_subscribersUrl forKey:kRepoSubscribersUrl];
    [aCoder encodeObject:_cloneUrl forKey:kRepoCloneUrl];
    [aCoder encodeBool:_hasWiki forKey:kRepoHasWiki];
    [aCoder encodeObject:_pullsUrl forKey:kRepoPullsUrl];
    [aCoder encodeObject:_url forKey:kRepoUrl];
    [aCoder encodeBool:_fork forKey:kRepoFork];
    [aCoder encodeObject:_notificationsUrl forKey:kRepoNotificationsUrl];
    [aCoder encodeObject:_repoDescription forKey:kRepoDescription];
    [aCoder encodeObject:_collaboratorsUrl forKey:kRepoCollaboratorsUrl];
    [aCoder encodeObject:_deploymentsUrl forKey:kRepoDeploymentsUrl];
    [aCoder encodeObject:_languagesUrl forKey:kRepoLanguagesUrl];
    [aCoder encodeBool:_hasIssues forKey:kRepoHasIssues];
    [aCoder encodeObject:_commentsUrl forKey:kRepoCommentsUrl];
    [aCoder encodeBool:_privateProperty forKey:kRepoPrivate];
    [aCoder encodeDouble:_size forKey:kRepoSize];
    [aCoder encodeObject:_gitTagsUrl forKey:kRepoGitTagsUrl];
    [aCoder encodeObject:_updatedAt forKey:kRepoUpdatedAt];
    [aCoder encodeObject:_sshUrl forKey:kRepoSshUrl];
    [aCoder encodeObject:_name forKey:kRepoName];
    [aCoder encodeObject:_archiveUrl forKey:kRepoArchiveUrl];
    [aCoder encodeDouble:_openIssuesCount forKey:kRepoOpenIssuesCount];
    [aCoder encodeObject:_milestonesUrl forKey:kRepoMilestonesUrl];
    [aCoder encodeObject:_blobsUrl forKey:kRepoBlobsUrl];
    [aCoder encodeObject:_contributorsUrl forKey:kRepoContributorsUrl];
    [aCoder encodeObject:_contentsUrl forKey:kRepoContentsUrl];
    [aCoder encodeDouble:_forksCount forKey:kRepoForksCount];
    [aCoder encodeObject:_treesUrl forKey:kRepoTreesUrl];
    [aCoder encodeObject:_mirrorUrl forKey:kRepoMirrorUrl];
    [aCoder encodeBool:_hasDownloads forKey:kRepoHasDownloads];
    [aCoder encodeObject:_createdAt forKey:kRepoCreatedAt];
    [aCoder encodeObject:_forksUrl forKey:kRepoForksUrl];
    [aCoder encodeObject:_svnUrl forKey:kRepoSvnUrl];
    [aCoder encodeObject:_commitsUrl forKey:kRepoCommitsUrl];
    [aCoder encodeObject:_homepage forKey:kRepoHomepage];
    [aCoder encodeObject:_teamsUrl forKey:kRepoTeamsUrl];
    [aCoder encodeObject:_branchesUrl forKey:kRepoBranchesUrl];
    [aCoder encodeObject:_issueCommentUrl forKey:kRepoIssueCommentUrl];
    [aCoder encodeObject:_mergesUrl forKey:kRepoMergesUrl];
    [aCoder encodeObject:_gitRefsUrl forKey:kRepoGitRefsUrl];
    [aCoder encodeObject:_gitUrl forKey:kRepoGitUrl];
    [aCoder encodeDouble:_forks forKey:kRepoForks];
    [aCoder encodeDouble:_openIssues forKey:kRepoOpenIssues];
    [aCoder encodeObject:_hooksUrl forKey:kRepoHooksUrl];
    [aCoder encodeObject:_htmlUrl forKey:kRepoHtmlUrl];
    [aCoder encodeObject:_stargazersUrl forKey:kRepoStargazersUrl];
    [aCoder encodeBool:_hasPages forKey:kRepoHasPages];
    [aCoder encodeObject:_assigneesUrl forKey:kRepoAssigneesUrl];
    [aCoder encodeObject:_compareUrl forKey:kRepoCompareUrl];
    [aCoder encodeObject:_fullName forKey:kRepoFullName];
    [aCoder encodeObject:_tagsUrl forKey:kRepoTagsUrl];
    [aCoder encodeObject:_releasesUrl forKey:kRepoReleasesUrl];
    [aCoder encodeObject:_pushedAt forKey:kRepoPushedAt];
    [aCoder encodeObject:_labelsUrl forKey:kRepoLabelsUrl];
    [aCoder encodeObject:_downloadsUrl forKey:kRepoDownloadsUrl];
    [aCoder encodeObject:_defaultBranch forKey:kRepoDefaultBranch];
    [aCoder encodeDouble:_stargazersCount forKey:kRepoStargazersCount];
    [aCoder encodeDouble:_watchersCount forKey:kRepoWatchersCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    Repo *copy = [[Repo alloc] init];
    
    if (copy) {

        copy.keysUrl = [self.keysUrl copyWithZone:zone];
        copy.statusesUrl = [self.statusesUrl copyWithZone:zone];
        copy.issuesUrl = [self.issuesUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.issueEventsUrl = [self.issueEventsUrl copyWithZone:zone];
        copy.repoIdentifier = self.repoIdentifier;
        copy.owner = [self.owner copyWithZone:zone];
        copy.eventsUrl = [self.eventsUrl copyWithZone:zone];
        copy.subscriptionUrl = [self.subscriptionUrl copyWithZone:zone];
        copy.watchers = self.watchers;
        copy.gitCommitsUrl = [self.gitCommitsUrl copyWithZone:zone];
        copy.subscribersUrl = [self.subscribersUrl copyWithZone:zone];
        copy.cloneUrl = [self.cloneUrl copyWithZone:zone];
        copy.hasWiki = self.hasWiki;
        copy.pullsUrl = [self.pullsUrl copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.fork = self.fork;
        copy.notificationsUrl = [self.notificationsUrl copyWithZone:zone];
        copy.repoDescription = [self.repoDescription copyWithZone:zone];
        copy.collaboratorsUrl = [self.collaboratorsUrl copyWithZone:zone];
        copy.deploymentsUrl = [self.deploymentsUrl copyWithZone:zone];
        copy.languagesUrl = [self.languagesUrl copyWithZone:zone];
        copy.hasIssues = self.hasIssues;
        copy.commentsUrl = [self.commentsUrl copyWithZone:zone];
        copy.privateProperty = self.privateProperty;
        copy.size = self.size;
        copy.gitTagsUrl = [self.gitTagsUrl copyWithZone:zone];
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.sshUrl = [self.sshUrl copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.archiveUrl = [self.archiveUrl copyWithZone:zone];
        copy.openIssuesCount = self.openIssuesCount;
        copy.milestonesUrl = [self.milestonesUrl copyWithZone:zone];
        copy.blobsUrl = [self.blobsUrl copyWithZone:zone];
        copy.contributorsUrl = [self.contributorsUrl copyWithZone:zone];
        copy.contentsUrl = [self.contentsUrl copyWithZone:zone];
        copy.forksCount = self.forksCount;
        copy.treesUrl = [self.treesUrl copyWithZone:zone];
        copy.mirrorUrl = [self.mirrorUrl copyWithZone:zone];
        copy.hasDownloads = self.hasDownloads;
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.forksUrl = [self.forksUrl copyWithZone:zone];
        copy.svnUrl = [self.svnUrl copyWithZone:zone];
        copy.commitsUrl = [self.commitsUrl copyWithZone:zone];
        copy.homepage = [self.homepage copyWithZone:zone];
        copy.teamsUrl = [self.teamsUrl copyWithZone:zone];
        copy.branchesUrl = [self.branchesUrl copyWithZone:zone];
        copy.issueCommentUrl = [self.issueCommentUrl copyWithZone:zone];
        copy.mergesUrl = [self.mergesUrl copyWithZone:zone];
        copy.gitRefsUrl = [self.gitRefsUrl copyWithZone:zone];
        copy.gitUrl = [self.gitUrl copyWithZone:zone];
        copy.forks = self.forks;
        copy.openIssues = self.openIssues;
        copy.hooksUrl = [self.hooksUrl copyWithZone:zone];
        copy.htmlUrl = [self.htmlUrl copyWithZone:zone];
        copy.stargazersUrl = [self.stargazersUrl copyWithZone:zone];
        copy.hasPages = self.hasPages;
        copy.assigneesUrl = [self.assigneesUrl copyWithZone:zone];
        copy.compareUrl = [self.compareUrl copyWithZone:zone];
        copy.fullName = [self.fullName copyWithZone:zone];
        copy.tagsUrl = [self.tagsUrl copyWithZone:zone];
        copy.releasesUrl = [self.releasesUrl copyWithZone:zone];
        copy.pushedAt = [self.pushedAt copyWithZone:zone];
        copy.labelsUrl = [self.labelsUrl copyWithZone:zone];
        copy.downloadsUrl = [self.downloadsUrl copyWithZone:zone];
        copy.defaultBranch = [self.defaultBranch copyWithZone:zone];
        copy.stargazersCount = self.stargazersCount;
        copy.watchersCount = self.watchersCount;
    }
    
    return copy;
}


@end
