//
//  Creator.m
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Creator.h"


NSString *const kCreatorId = @"id";
NSString *const kCreatorOrganizationsUrl = @"organizations_url";
NSString *const kCreatorReceivedEventsUrl = @"received_events_url";
NSString *const kCreatorFollowingUrl = @"following_url";
NSString *const kCreatorLogin = @"login";
NSString *const kCreatorSubscriptionsUrl = @"subscriptions_url";
NSString *const kCreatorAvatarUrl = @"avatar_url";
NSString *const kCreatorUrl = @"url";
NSString *const kCreatorType = @"type";
NSString *const kCreatorReposUrl = @"repos_url";
NSString *const kCreatorHtmlUrl = @"html_url";
NSString *const kCreatorEventsUrl = @"events_url";
NSString *const kCreatorSiteAdmin = @"site_admin";
NSString *const kCreatorStarredUrl = @"starred_url";
NSString *const kCreatorGistsUrl = @"gists_url";
NSString *const kCreatorGravatarId = @"gravatar_id";
NSString *const kCreatorFollowersUrl = @"followers_url";


@interface Creator ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Creator

@synthesize creatorIdentifier = _creatorIdentifier;
@synthesize organizationsUrl = _organizationsUrl;
@synthesize receivedEventsUrl = _receivedEventsUrl;
@synthesize followingUrl = _followingUrl;
@synthesize login = _login;
@synthesize subscriptionsUrl = _subscriptionsUrl;
@synthesize avatarUrl = _avatarUrl;
@synthesize url = _url;
@synthesize type = _type;
@synthesize reposUrl = _reposUrl;
@synthesize htmlUrl = _htmlUrl;
@synthesize eventsUrl = _eventsUrl;
@synthesize siteAdmin = _siteAdmin;
@synthesize starredUrl = _starredUrl;
@synthesize gistsUrl = _gistsUrl;
@synthesize gravatarId = _gravatarId;
@synthesize followersUrl = _followersUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.creatorIdentifier = [[self objectOrNilForKey:kCreatorId fromDictionary:dict] doubleValue];
            self.organizationsUrl = [self objectOrNilForKey:kCreatorOrganizationsUrl fromDictionary:dict];
            self.receivedEventsUrl = [self objectOrNilForKey:kCreatorReceivedEventsUrl fromDictionary:dict];
            self.followingUrl = [self objectOrNilForKey:kCreatorFollowingUrl fromDictionary:dict];
            self.login = [self objectOrNilForKey:kCreatorLogin fromDictionary:dict];
            self.subscriptionsUrl = [self objectOrNilForKey:kCreatorSubscriptionsUrl fromDictionary:dict];
            self.avatarUrl = [self objectOrNilForKey:kCreatorAvatarUrl fromDictionary:dict];
            self.url = [self objectOrNilForKey:kCreatorUrl fromDictionary:dict];
            self.type = [self objectOrNilForKey:kCreatorType fromDictionary:dict];
            self.reposUrl = [self objectOrNilForKey:kCreatorReposUrl fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kCreatorHtmlUrl fromDictionary:dict];
            self.eventsUrl = [self objectOrNilForKey:kCreatorEventsUrl fromDictionary:dict];
            self.siteAdmin = [[self objectOrNilForKey:kCreatorSiteAdmin fromDictionary:dict] boolValue];
            self.starredUrl = [self objectOrNilForKey:kCreatorStarredUrl fromDictionary:dict];
            self.gistsUrl = [self objectOrNilForKey:kCreatorGistsUrl fromDictionary:dict];
            self.gravatarId = [self objectOrNilForKey:kCreatorGravatarId fromDictionary:dict];
            self.followersUrl = [self objectOrNilForKey:kCreatorFollowersUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.creatorIdentifier] forKey:kCreatorId];
    [mutableDict setValue:self.organizationsUrl forKey:kCreatorOrganizationsUrl];
    [mutableDict setValue:self.receivedEventsUrl forKey:kCreatorReceivedEventsUrl];
    [mutableDict setValue:self.followingUrl forKey:kCreatorFollowingUrl];
    [mutableDict setValue:self.login forKey:kCreatorLogin];
    [mutableDict setValue:self.subscriptionsUrl forKey:kCreatorSubscriptionsUrl];
    [mutableDict setValue:self.avatarUrl forKey:kCreatorAvatarUrl];
    [mutableDict setValue:self.url forKey:kCreatorUrl];
    [mutableDict setValue:self.type forKey:kCreatorType];
    [mutableDict setValue:self.reposUrl forKey:kCreatorReposUrl];
    [mutableDict setValue:self.htmlUrl forKey:kCreatorHtmlUrl];
    [mutableDict setValue:self.eventsUrl forKey:kCreatorEventsUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.siteAdmin] forKey:kCreatorSiteAdmin];
    [mutableDict setValue:self.starredUrl forKey:kCreatorStarredUrl];
    [mutableDict setValue:self.gistsUrl forKey:kCreatorGistsUrl];
    [mutableDict setValue:self.gravatarId forKey:kCreatorGravatarId];
    [mutableDict setValue:self.followersUrl forKey:kCreatorFollowersUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.creatorIdentifier = [aDecoder decodeDoubleForKey:kCreatorId];
    self.organizationsUrl = [aDecoder decodeObjectForKey:kCreatorOrganizationsUrl];
    self.receivedEventsUrl = [aDecoder decodeObjectForKey:kCreatorReceivedEventsUrl];
    self.followingUrl = [aDecoder decodeObjectForKey:kCreatorFollowingUrl];
    self.login = [aDecoder decodeObjectForKey:kCreatorLogin];
    self.subscriptionsUrl = [aDecoder decodeObjectForKey:kCreatorSubscriptionsUrl];
    self.avatarUrl = [aDecoder decodeObjectForKey:kCreatorAvatarUrl];
    self.url = [aDecoder decodeObjectForKey:kCreatorUrl];
    self.type = [aDecoder decodeObjectForKey:kCreatorType];
    self.reposUrl = [aDecoder decodeObjectForKey:kCreatorReposUrl];
    self.htmlUrl = [aDecoder decodeObjectForKey:kCreatorHtmlUrl];
    self.eventsUrl = [aDecoder decodeObjectForKey:kCreatorEventsUrl];
    self.siteAdmin = [aDecoder decodeBoolForKey:kCreatorSiteAdmin];
    self.starredUrl = [aDecoder decodeObjectForKey:kCreatorStarredUrl];
    self.gistsUrl = [aDecoder decodeObjectForKey:kCreatorGistsUrl];
    self.gravatarId = [aDecoder decodeObjectForKey:kCreatorGravatarId];
    self.followersUrl = [aDecoder decodeObjectForKey:kCreatorFollowersUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_creatorIdentifier forKey:kCreatorId];
    [aCoder encodeObject:_organizationsUrl forKey:kCreatorOrganizationsUrl];
    [aCoder encodeObject:_receivedEventsUrl forKey:kCreatorReceivedEventsUrl];
    [aCoder encodeObject:_followingUrl forKey:kCreatorFollowingUrl];
    [aCoder encodeObject:_login forKey:kCreatorLogin];
    [aCoder encodeObject:_subscriptionsUrl forKey:kCreatorSubscriptionsUrl];
    [aCoder encodeObject:_avatarUrl forKey:kCreatorAvatarUrl];
    [aCoder encodeObject:_url forKey:kCreatorUrl];
    [aCoder encodeObject:_type forKey:kCreatorType];
    [aCoder encodeObject:_reposUrl forKey:kCreatorReposUrl];
    [aCoder encodeObject:_htmlUrl forKey:kCreatorHtmlUrl];
    [aCoder encodeObject:_eventsUrl forKey:kCreatorEventsUrl];
    [aCoder encodeBool:_siteAdmin forKey:kCreatorSiteAdmin];
    [aCoder encodeObject:_starredUrl forKey:kCreatorStarredUrl];
    [aCoder encodeObject:_gistsUrl forKey:kCreatorGistsUrl];
    [aCoder encodeObject:_gravatarId forKey:kCreatorGravatarId];
    [aCoder encodeObject:_followersUrl forKey:kCreatorFollowersUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    Creator *copy = [[Creator alloc] init];
    
    if (copy) {

        copy.creatorIdentifier = self.creatorIdentifier;
        copy.organizationsUrl = [self.organizationsUrl copyWithZone:zone];
        copy.receivedEventsUrl = [self.receivedEventsUrl copyWithZone:zone];
        copy.followingUrl = [self.followingUrl copyWithZone:zone];
        copy.login = [self.login copyWithZone:zone];
        copy.subscriptionsUrl = [self.subscriptionsUrl copyWithZone:zone];
        copy.avatarUrl = [self.avatarUrl copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.type = [self.type copyWithZone:zone];
        copy.reposUrl = [self.reposUrl copyWithZone:zone];
        copy.htmlUrl = [self.htmlUrl copyWithZone:zone];
        copy.eventsUrl = [self.eventsUrl copyWithZone:zone];
        copy.siteAdmin = self.siteAdmin;
        copy.starredUrl = [self.starredUrl copyWithZone:zone];
        copy.gistsUrl = [self.gistsUrl copyWithZone:zone];
        copy.gravatarId = [self.gravatarId copyWithZone:zone];
        copy.followersUrl = [self.followersUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
