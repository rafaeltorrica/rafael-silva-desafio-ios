//
//  BaseClass.m
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "BaseClass.h"
#import "Items.h"
#import "Base.h"
#import "User.h"
#import "Head.h"
#import "Links.h"


NSString *const kBaseClassMilestone = @"milestone";
NSString *const kBaseClassLocked = @"locked";
NSString *const kBaseClassTitle = @"title";
NSString *const kBaseClassUrl = @"url";
NSString *const kBaseClassCommitsUrl = @"commits_url";
NSString *const kBaseClassMergeCommitSha = @"merge_commit_sha";
NSString *const kBaseClassReviewCommentUrl = @"review_comment_url";
NSString *const kBaseClassUpdatedAt = @"updated_at";
NSString *const kBaseClassBase = @"base";
NSString *const kBaseClassReviewCommentsUrl = @"review_comments_url";
NSString *const kBaseClassAssignee = @"assignee";
NSString *const kBaseClassCommentsUrl = @"comments_url";
NSString *const kBaseClassPatchUrl = @"patch_url";
NSString *const kBaseClassMergedAt = @"merged_at";
NSString *const kBaseClassState = @"state";
NSString *const kBaseClassBody = @"body";
NSString *const kBaseClassId = @"id";
NSString *const kBaseClassNumber = @"number";
NSString *const kBaseClassIssueUrl = @"issue_url";
NSString *const kBaseClassUser = @"user";
NSString *const kBaseClassClosedAt = @"closed_at";
NSString *const kBaseClassHead = @"head";
NSString *const kBaseClassAssignees = @"assignees";
NSString *const kBaseClassStatusesUrl = @"statuses_url";
NSString *const kBaseClassCreatedAt = @"created_at";
NSString *const kBaseClassLinks = @"_links";
NSString *const kBaseClassDiffUrl = @"diff_url";
NSString *const kBaseClassHtmlUrl = @"html_url";
NSString *const kBaseClassTotalCount = @"total_count";
NSString *const kBaseClassIncompleteResults = @"incomplete_results";
NSString *const kBaseClassItems = @"items";


@interface BaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BaseClass

@synthesize totalCount = _totalCount;
@synthesize incompleteResults = _incompleteResults;
@synthesize items = _items;
@synthesize milestone = _milestone;
@synthesize locked = _locked;
@synthesize title = _title;
@synthesize url = _url;
@synthesize commitsUrl = _commitsUrl;
@synthesize mergeCommitSha = _mergeCommitSha;
@synthesize reviewCommentUrl = _reviewCommentUrl;
@synthesize updatedAt = _updatedAt;
@synthesize base = _base;
@synthesize reviewCommentsUrl = _reviewCommentsUrl;
@synthesize assignee = _assignee;
@synthesize commentsUrl = _commentsUrl;
@synthesize patchUrl = _patchUrl;
@synthesize mergedAt = _mergedAt;
@synthesize state = _state;
@synthesize body = _body;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize number = _number;
@synthesize issueUrl = _issueUrl;
@synthesize user = _user;
@synthesize closedAt = _closedAt;
@synthesize head = _head;
@synthesize assignees = _assignees;
@synthesize statusesUrl = _statusesUrl;
@synthesize createdAt = _createdAt;
@synthesize links = _links;
@synthesize diffUrl = _diffUrl;
@synthesize htmlUrl = _htmlUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.totalCount = [[self objectOrNilForKey:kBaseClassTotalCount fromDictionary:dict] doubleValue];
            self.incompleteResults = [[self objectOrNilForKey:kBaseClassIncompleteResults fromDictionary:dict] boolValue];
    NSObject *receivedItems = [dict objectForKey:kBaseClassItems];
    NSMutableArray *parsedItems = [NSMutableArray array];
    if ([receivedItems isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedItems) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedItems addObject:[Items modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedItems isKindOfClass:[NSDictionary class]]) {
       [parsedItems addObject:[Items modelObjectWithDictionary:(NSDictionary *)receivedItems]];
    }

    self.items = [NSArray arrayWithArray:parsedItems];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalCount] forKey:kBaseClassTotalCount];
    [mutableDict setValue:[NSNumber numberWithBool:self.incompleteResults] forKey:kBaseClassIncompleteResults];
    NSMutableArray *tempArrayForItems = [NSMutableArray array];
    for (NSObject *subArrayObject in self.items) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForItems addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForItems addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForItems] forKey:kBaseClassItems];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.milestone = [aDecoder decodeObjectForKey:kBaseClassMilestone];
    self.locked = [aDecoder decodeBoolForKey:kBaseClassLocked];
    self.title = [aDecoder decodeObjectForKey:kBaseClassTitle];
    self.url = [aDecoder decodeObjectForKey:kBaseClassUrl];
    self.commitsUrl = [aDecoder decodeObjectForKey:kBaseClassCommitsUrl];
    self.mergeCommitSha = [aDecoder decodeObjectForKey:kBaseClassMergeCommitSha];
    self.reviewCommentUrl = [aDecoder decodeObjectForKey:kBaseClassReviewCommentUrl];
    self.updatedAt = [aDecoder decodeObjectForKey:kBaseClassUpdatedAt];
    self.base = [aDecoder decodeObjectForKey:kBaseClassBase];
    self.reviewCommentsUrl = [aDecoder decodeObjectForKey:kBaseClassReviewCommentsUrl];
    self.assignee = [aDecoder decodeObjectForKey:kBaseClassAssignee];
    self.commentsUrl = [aDecoder decodeObjectForKey:kBaseClassCommentsUrl];
    self.patchUrl = [aDecoder decodeObjectForKey:kBaseClassPatchUrl];
    self.mergedAt = [aDecoder decodeObjectForKey:kBaseClassMergedAt];
    self.state = [aDecoder decodeObjectForKey:kBaseClassState];
    self.body = [aDecoder decodeObjectForKey:kBaseClassBody];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kBaseClassId];
    self.number = [aDecoder decodeDoubleForKey:kBaseClassNumber];
    self.issueUrl = [aDecoder decodeObjectForKey:kBaseClassIssueUrl];
    self.user = [aDecoder decodeObjectForKey:kBaseClassUser];
    self.closedAt = [aDecoder decodeObjectForKey:kBaseClassClosedAt];
    self.head = [aDecoder decodeObjectForKey:kBaseClassHead];
    self.assignees = [aDecoder decodeObjectForKey:kBaseClassAssignees];
    self.statusesUrl = [aDecoder decodeObjectForKey:kBaseClassStatusesUrl];
    self.createdAt = [aDecoder decodeObjectForKey:kBaseClassCreatedAt];
    self.links = [aDecoder decodeObjectForKey:kBaseClassLinks];
    self.diffUrl = [aDecoder decodeObjectForKey:kBaseClassDiffUrl];
    self.htmlUrl = [aDecoder decodeObjectForKey:kBaseClassHtmlUrl];
    self.totalCount = [aDecoder decodeDoubleForKey:kBaseClassTotalCount];
    self.incompleteResults = [aDecoder decodeBoolForKey:kBaseClassIncompleteResults];
    self.items = [aDecoder decodeObjectForKey:kBaseClassItems];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_milestone forKey:kBaseClassMilestone];
    [aCoder encodeBool:_locked forKey:kBaseClassLocked];
    [aCoder encodeObject:_title forKey:kBaseClassTitle];
    [aCoder encodeObject:_url forKey:kBaseClassUrl];
    [aCoder encodeObject:_commitsUrl forKey:kBaseClassCommitsUrl];
    [aCoder encodeObject:_mergeCommitSha forKey:kBaseClassMergeCommitSha];
    [aCoder encodeObject:_reviewCommentUrl forKey:kBaseClassReviewCommentUrl];
    [aCoder encodeObject:_updatedAt forKey:kBaseClassUpdatedAt];
    [aCoder encodeObject:_base forKey:kBaseClassBase];
    [aCoder encodeObject:_reviewCommentsUrl forKey:kBaseClassReviewCommentsUrl];
    [aCoder encodeObject:_assignee forKey:kBaseClassAssignee];
    [aCoder encodeObject:_commentsUrl forKey:kBaseClassCommentsUrl];
    [aCoder encodeObject:_patchUrl forKey:kBaseClassPatchUrl];
    [aCoder encodeObject:_mergedAt forKey:kBaseClassMergedAt];
    [aCoder encodeObject:_state forKey:kBaseClassState];
    [aCoder encodeObject:_body forKey:kBaseClassBody];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kBaseClassId];
    [aCoder encodeDouble:_number forKey:kBaseClassNumber];
    [aCoder encodeObject:_issueUrl forKey:kBaseClassIssueUrl];
    [aCoder encodeObject:_user forKey:kBaseClassUser];
    [aCoder encodeObject:_closedAt forKey:kBaseClassClosedAt];
    [aCoder encodeObject:_head forKey:kBaseClassHead];
    [aCoder encodeObject:_assignees forKey:kBaseClassAssignees];
    [aCoder encodeObject:_statusesUrl forKey:kBaseClassStatusesUrl];
    [aCoder encodeObject:_createdAt forKey:kBaseClassCreatedAt];
    [aCoder encodeObject:_links forKey:kBaseClassLinks];
    [aCoder encodeObject:_diffUrl forKey:kBaseClassDiffUrl];
    [aCoder encodeObject:_htmlUrl forKey:kBaseClassHtmlUrl];
    [aCoder encodeDouble:_totalCount forKey:kBaseClassTotalCount];
    [aCoder encodeBool:_incompleteResults forKey:kBaseClassIncompleteResults];
    [aCoder encodeObject:_items forKey:kBaseClassItems];
}

- (id)copyWithZone:(NSZone *)zone
{
    BaseClass *copy = [[BaseClass alloc] init];
    
    if (copy) {

        copy.totalCount = self.totalCount;
        copy.incompleteResults = self.incompleteResults;
        copy.items = [self.items copyWithZone:zone];
    }
    
    return copy;
}


@end
