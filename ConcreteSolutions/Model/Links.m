//
//  Links.m
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Links.h"
#import "Comments.h"
#import "Issue.h"
#import "Commits.h"
#import "Html.h"
#import "Statuses.h"
#import "SelfClass.h"
#import "ReviewComment.h"
#import "ReviewComments.h"


NSString *const kLinksComments = @"comments";
NSString *const kLinksIssue = @"issue";
NSString *const kLinksCommits = @"commits";
NSString *const kLinksHtml = @"html";
NSString *const kLinksStatuses = @"statuses";
NSString *const kLinksSelf = @"self";
NSString *const kLinksReviewComment = @"review_comment";
NSString *const kLinksReviewComments = @"review_comments";


@interface Links ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Links

@synthesize comments = _comments;
@synthesize issue = _issue;
@synthesize commits = _commits;
@synthesize html = _html;
@synthesize statuses = _statuses;
@synthesize linksSelf = _linksSelf;
@synthesize reviewComment = _reviewComment;
@synthesize reviewComments = _reviewComments;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.comments = [Comments modelObjectWithDictionary:[dict objectForKey:kLinksComments]];
            self.issue = [Issue modelObjectWithDictionary:[dict objectForKey:kLinksIssue]];
            self.commits = [Commits modelObjectWithDictionary:[dict objectForKey:kLinksCommits]];
            self.html = [Html modelObjectWithDictionary:[dict objectForKey:kLinksHtml]];
            self.statuses = [Statuses modelObjectWithDictionary:[dict objectForKey:kLinksStatuses]];
            self.linksSelf = [SelfClass modelObjectWithDictionary:[dict objectForKey:kLinksSelf]];
            self.reviewComment = [ReviewComment modelObjectWithDictionary:[dict objectForKey:kLinksReviewComment]];
            self.reviewComments = [ReviewComments modelObjectWithDictionary:[dict objectForKey:kLinksReviewComments]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.comments dictionaryRepresentation] forKey:kLinksComments];
    [mutableDict setValue:[self.issue dictionaryRepresentation] forKey:kLinksIssue];
    [mutableDict setValue:[self.commits dictionaryRepresentation] forKey:kLinksCommits];
    [mutableDict setValue:[self.html dictionaryRepresentation] forKey:kLinksHtml];
    [mutableDict setValue:[self.statuses dictionaryRepresentation] forKey:kLinksStatuses];
    [mutableDict setValue:[self.linksSelf dictionaryRepresentation] forKey:kLinksSelf];
    [mutableDict setValue:[self.reviewComment dictionaryRepresentation] forKey:kLinksReviewComment];
    [mutableDict setValue:[self.reviewComments dictionaryRepresentation] forKey:kLinksReviewComments];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.comments = [aDecoder decodeObjectForKey:kLinksComments];
    self.issue = [aDecoder decodeObjectForKey:kLinksIssue];
    self.commits = [aDecoder decodeObjectForKey:kLinksCommits];
    self.html = [aDecoder decodeObjectForKey:kLinksHtml];
    self.statuses = [aDecoder decodeObjectForKey:kLinksStatuses];
    self.linksSelf = [aDecoder decodeObjectForKey:kLinksSelf];
    self.reviewComment = [aDecoder decodeObjectForKey:kLinksReviewComment];
    self.reviewComments = [aDecoder decodeObjectForKey:kLinksReviewComments];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_comments forKey:kLinksComments];
    [aCoder encodeObject:_issue forKey:kLinksIssue];
    [aCoder encodeObject:_commits forKey:kLinksCommits];
    [aCoder encodeObject:_html forKey:kLinksHtml];
    [aCoder encodeObject:_statuses forKey:kLinksStatuses];
    [aCoder encodeObject:_linksSelf forKey:kLinksSelf];
    [aCoder encodeObject:_reviewComment forKey:kLinksReviewComment];
    [aCoder encodeObject:_reviewComments forKey:kLinksReviewComments];
}

- (id)copyWithZone:(NSZone *)zone
{
    Links *copy = [[Links alloc] init];
    
    if (copy) {

        copy.comments = [self.comments copyWithZone:zone];
        copy.issue = [self.issue copyWithZone:zone];
        copy.commits = [self.commits copyWithZone:zone];
        copy.html = [self.html copyWithZone:zone];
        copy.statuses = [self.statuses copyWithZone:zone];
        copy.linksSelf = [self.linksSelf copyWithZone:zone];
        copy.reviewComment = [self.reviewComment copyWithZone:zone];
        copy.reviewComments = [self.reviewComments copyWithZone:zone];
    }
    
    return copy;
}


@end
