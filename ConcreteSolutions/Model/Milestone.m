//
//  Milestone.m
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Milestone.h"
#import "Creator.h"


NSString *const kMilestoneId = @"id";
NSString *const kMilestoneDescription = @"description";
NSString *const kMilestoneOpenIssues = @"open_issues";
NSString *const kMilestoneState = @"state";
NSString *const kMilestoneCreatedAt = @"created_at";
NSString *const kMilestoneLabelsUrl = @"labels_url";
NSString *const kMilestoneUrl = @"url";
NSString *const kMilestoneClosedIssues = @"closed_issues";
NSString *const kMilestoneTitle = @"title";
NSString *const kMilestoneCreator = @"creator";
NSString *const kMilestoneHtmlUrl = @"html_url";
NSString *const kMilestoneNumber = @"number";
NSString *const kMilestoneUpdatedAt = @"updated_at";
NSString *const kMilestoneDueOn = @"due_on";
NSString *const kMilestoneClosedAt = @"closed_at";


@interface Milestone ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Milestone

@synthesize milestoneIdentifier = _milestoneIdentifier;
@synthesize milestoneDescription = _milestoneDescription;
@synthesize openIssues = _openIssues;
@synthesize state = _state;
@synthesize createdAt = _createdAt;
@synthesize labelsUrl = _labelsUrl;
@synthesize url = _url;
@synthesize closedIssues = _closedIssues;
@synthesize title = _title;
@synthesize creator = _creator;
@synthesize htmlUrl = _htmlUrl;
@synthesize number = _number;
@synthesize updatedAt = _updatedAt;
@synthesize dueOn = _dueOn;
@synthesize closedAt = _closedAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.milestoneIdentifier = [[self objectOrNilForKey:kMilestoneId fromDictionary:dict] doubleValue];
            self.milestoneDescription = [self objectOrNilForKey:kMilestoneDescription fromDictionary:dict];
            self.openIssues = [[self objectOrNilForKey:kMilestoneOpenIssues fromDictionary:dict] doubleValue];
            self.state = [self objectOrNilForKey:kMilestoneState fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kMilestoneCreatedAt fromDictionary:dict];
            self.labelsUrl = [self objectOrNilForKey:kMilestoneLabelsUrl fromDictionary:dict];
            self.url = [self objectOrNilForKey:kMilestoneUrl fromDictionary:dict];
            self.closedIssues = [[self objectOrNilForKey:kMilestoneClosedIssues fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kMilestoneTitle fromDictionary:dict];
            self.creator = [Creator modelObjectWithDictionary:[dict objectForKey:kMilestoneCreator]];
            self.htmlUrl = [self objectOrNilForKey:kMilestoneHtmlUrl fromDictionary:dict];
            self.number = [[self objectOrNilForKey:kMilestoneNumber fromDictionary:dict] doubleValue];
            self.updatedAt = [self objectOrNilForKey:kMilestoneUpdatedAt fromDictionary:dict];
            self.dueOn = [self objectOrNilForKey:kMilestoneDueOn fromDictionary:dict];
            self.closedAt = [self objectOrNilForKey:kMilestoneClosedAt fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.milestoneIdentifier] forKey:kMilestoneId];
    [mutableDict setValue:self.milestoneDescription forKey:kMilestoneDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.openIssues] forKey:kMilestoneOpenIssues];
    [mutableDict setValue:self.state forKey:kMilestoneState];
    [mutableDict setValue:self.createdAt forKey:kMilestoneCreatedAt];
    [mutableDict setValue:self.labelsUrl forKey:kMilestoneLabelsUrl];
    [mutableDict setValue:self.url forKey:kMilestoneUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.closedIssues] forKey:kMilestoneClosedIssues];
    [mutableDict setValue:self.title forKey:kMilestoneTitle];
    [mutableDict setValue:[self.creator dictionaryRepresentation] forKey:kMilestoneCreator];
    [mutableDict setValue:self.htmlUrl forKey:kMilestoneHtmlUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.number] forKey:kMilestoneNumber];
    [mutableDict setValue:self.updatedAt forKey:kMilestoneUpdatedAt];
    [mutableDict setValue:self.dueOn forKey:kMilestoneDueOn];
    [mutableDict setValue:self.closedAt forKey:kMilestoneClosedAt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.milestoneIdentifier = [aDecoder decodeDoubleForKey:kMilestoneId];
    self.milestoneDescription = [aDecoder decodeObjectForKey:kMilestoneDescription];
    self.openIssues = [aDecoder decodeDoubleForKey:kMilestoneOpenIssues];
    self.state = [aDecoder decodeObjectForKey:kMilestoneState];
    self.createdAt = [aDecoder decodeObjectForKey:kMilestoneCreatedAt];
    self.labelsUrl = [aDecoder decodeObjectForKey:kMilestoneLabelsUrl];
    self.url = [aDecoder decodeObjectForKey:kMilestoneUrl];
    self.closedIssues = [aDecoder decodeDoubleForKey:kMilestoneClosedIssues];
    self.title = [aDecoder decodeObjectForKey:kMilestoneTitle];
    self.creator = [aDecoder decodeObjectForKey:kMilestoneCreator];
    self.htmlUrl = [aDecoder decodeObjectForKey:kMilestoneHtmlUrl];
    self.number = [aDecoder decodeDoubleForKey:kMilestoneNumber];
    self.updatedAt = [aDecoder decodeObjectForKey:kMilestoneUpdatedAt];
    self.dueOn = [aDecoder decodeObjectForKey:kMilestoneDueOn];
    self.closedAt = [aDecoder decodeObjectForKey:kMilestoneClosedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_milestoneIdentifier forKey:kMilestoneId];
    [aCoder encodeObject:_milestoneDescription forKey:kMilestoneDescription];
    [aCoder encodeDouble:_openIssues forKey:kMilestoneOpenIssues];
    [aCoder encodeObject:_state forKey:kMilestoneState];
    [aCoder encodeObject:_createdAt forKey:kMilestoneCreatedAt];
    [aCoder encodeObject:_labelsUrl forKey:kMilestoneLabelsUrl];
    [aCoder encodeObject:_url forKey:kMilestoneUrl];
    [aCoder encodeDouble:_closedIssues forKey:kMilestoneClosedIssues];
    [aCoder encodeObject:_title forKey:kMilestoneTitle];
    [aCoder encodeObject:_creator forKey:kMilestoneCreator];
    [aCoder encodeObject:_htmlUrl forKey:kMilestoneHtmlUrl];
    [aCoder encodeDouble:_number forKey:kMilestoneNumber];
    [aCoder encodeObject:_updatedAt forKey:kMilestoneUpdatedAt];
    [aCoder encodeObject:_dueOn forKey:kMilestoneDueOn];
    [aCoder encodeObject:_closedAt forKey:kMilestoneClosedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    Milestone *copy = [[Milestone alloc] init];
    
    if (copy) {

        copy.milestoneIdentifier = self.milestoneIdentifier;
        copy.milestoneDescription = [self.milestoneDescription copyWithZone:zone];
        copy.openIssues = self.openIssues;
        copy.state = [self.state copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.labelsUrl = [self.labelsUrl copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.closedIssues = self.closedIssues;
        copy.title = [self.title copyWithZone:zone];
        copy.creator = [self.creator copyWithZone:zone];
        copy.htmlUrl = [self.htmlUrl copyWithZone:zone];
        copy.number = self.number;
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.dueOn = [self.dueOn copyWithZone:zone];
        copy.closedAt = [self.closedAt copyWithZone:zone];
    }
    
    return copy;
}


@end
