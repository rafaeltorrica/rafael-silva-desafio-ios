//
//  Base.m
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Base.h"
#import "Repo.h"
#import "User.h"


NSString *const kBaseRef = @"ref";
NSString *const kBaseLabel = @"label";
NSString *const kBaseSha = @"sha";
NSString *const kBaseRepo = @"repo";
NSString *const kBaseUser = @"user";


@interface Base ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Base

@synthesize ref = _ref;
@synthesize label = _label;
@synthesize sha = _sha;
@synthesize repo = _repo;
@synthesize user = _user;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ref = [self objectOrNilForKey:kBaseRef fromDictionary:dict];
            self.label = [self objectOrNilForKey:kBaseLabel fromDictionary:dict];
            self.sha = [self objectOrNilForKey:kBaseSha fromDictionary:dict];
            self.repo = [Repo modelObjectWithDictionary:[dict objectForKey:kBaseRepo]];
            self.user = [User modelObjectWithDictionary:[dict objectForKey:kBaseUser]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.ref forKey:kBaseRef];
    [mutableDict setValue:self.label forKey:kBaseLabel];
    [mutableDict setValue:self.sha forKey:kBaseSha];
    [mutableDict setValue:[self.repo dictionaryRepresentation] forKey:kBaseRepo];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kBaseUser];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.ref = [aDecoder decodeObjectForKey:kBaseRef];
    self.label = [aDecoder decodeObjectForKey:kBaseLabel];
    self.sha = [aDecoder decodeObjectForKey:kBaseSha];
    self.repo = [aDecoder decodeObjectForKey:kBaseRepo];
    self.user = [aDecoder decodeObjectForKey:kBaseUser];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_ref forKey:kBaseRef];
    [aCoder encodeObject:_label forKey:kBaseLabel];
    [aCoder encodeObject:_sha forKey:kBaseSha];
    [aCoder encodeObject:_repo forKey:kBaseRepo];
    [aCoder encodeObject:_user forKey:kBaseUser];
}

- (id)copyWithZone:(NSZone *)zone
{
    Base *copy = [[Base alloc] init];
    
    if (copy) {

        copy.ref = [self.ref copyWithZone:zone];
        copy.label = [self.label copyWithZone:zone];
        copy.sha = [self.sha copyWithZone:zone];
        copy.repo = [self.repo copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
    }
    
    return copy;
}


@end
