//
//  Base.h
//
//  Created by   on 12/28/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Repo, User;

@interface Base : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *ref;
@property (nonatomic, strong) NSString *label;
@property (nonatomic, strong) NSString *sha;
@property (nonatomic, strong) Repo *repo;
@property (nonatomic, strong) User *user;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
