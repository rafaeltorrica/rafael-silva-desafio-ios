
//  ServerDelegate.m
//
//  Created by Douglas Amengual Severo on 06/06/13.
//  Copyright (c) 2013 Sevdroid Ltda. All rights reserved.
//

#import "ServerDelegate.h"
#import "SBJson.h"

@implementation ServerDelegate{}


static NSString *URL_ADMIN=@"api.github.com/search/repositories?q=language:Java&sort=stars&page=";

static NSString *kREST = @"rest";

////---------------------------------------


-(void)getJson:(id)sender withParam:(NSString *) param andOKSelector:(SEL)_selector andFAILSelector:(SEL)_failSelector{
    
    listener = sender;
    selector = _selector;
    failSelector = _failSelector;
    
    isRetornoData = FALSE;
    chamada = @"get";
    origem = kREST;
    
    [self chamaHttpGet:sender withURL:param andProtocol:@"http" andServer:URL_ADMIN andOrigin:origem];
}

-(void)jsonPost: (id) sender withParam:(NSDictionary *) param andURL:(NSString *) url andOKSelector: (SEL) _selector andFAILSelector: (SEL) _failSelector {
    
    listener = sender;
    selector = _selector;
    failSelector = _failSelector;
    
    isRetornoData = FALSE;
    chamada = @"jsonPost";
    origem = @"token";
    
    
    [self chamaHttp:sender withMethod:@"POST" andURL:url andDictionary:param andProtocol:@"http" andServer:URL_ADMIN andOrigin:origem];
}

//------------------------------------


//// --------------------------------------

//-----------------------------------------
-(void) chamaHttpGet: (id) sender withURL: (NSString *) url andProtocol: (NSString *) protocol andServer:(NSString *) server andOrigin:(NSString *) origin {

    NSLog(@"chama HTTP GET --------------- ");
    NSLog(@"URL: %@", url);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlCompleta = [NSString stringWithFormat:@"%@://%@%@",protocol, server, url];
    [request setURL:[NSURL URLWithString:urlCompleta]];
    
    NSLog(@"urlCompleta - %@",urlCompleta);
    
    
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];

   
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    
    NSLog(@"CONNECTION: %@", conn);


}
//-----------------


//-----------------
-(void) chamaHttp: (id) sender withMethod:(NSString *) method andURL: (NSString *) url andDictionary: (NSDictionary *) dic andProtocol: (NSString *) protocol andServer:(NSString *) server andOrigin:(NSString *) origin {
    
    NSLog(@"chama HTTP POST --------------- ");
    NSLog(@"URL: %@", url);
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlCompleta = [NSString stringWithFormat:@"%@://%@%@",protocol, server, url];
    [request setURL:[NSURL URLWithString:urlCompleta]];
    
    NSLog(@"urlCompleta - %@",urlCompleta);
    
    NSMutableDictionary *dicEnvio = [[NSMutableDictionary alloc] init];
    [dicEnvio addEntriesFromDictionary:dic];
    
    NSString *string = [dicEnvio JSONRepresentation];
    NSLog(@"dados p/ envio - %@",string);
    NSData *postData = [string dataUsingEncoding:NSUTF8StringEncoding];

 
    [request setTimeoutInterval:60];
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    if (postData) {
        NSString *postLength = [NSString stringWithFormat:@"%lu", (long)[postData length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
    }
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];

    NSLog(@"CONNECTION: %@", conn);
    
    
}
//---------------
-(void) chamaHttpSendMessage: (id) sender withMethod:(NSString *) method andURL: (NSString *) url andDictionary: (NSDictionary *) dic andProtocol: (NSString *) protocol andServer:(NSString *) server andOrigin:(NSString *) origin {
    
    NSLog(@"chama HTTP POST --------------- ");
    NSLog(@"URL: %@", url);
    
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlCompleta = [NSString stringWithFormat:@"%@://%@%@",protocol, server, url];
    [request setURL:[NSURL URLWithString:urlCompleta]];
    
    NSLog(@"urlCompleta - %@",urlCompleta);
    
    NSMutableDictionary *dicEnvio = [[NSMutableDictionary alloc] init];
    [dicEnvio addEntriesFromDictionary:dic];
    
    NSString *string = [dicEnvio JSONRepresentation];
    NSLog(@"dados p/ envio - %@",string);
    NSData *postData = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    
    [request setTimeoutInterval:60];
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    if (postData) {
        NSString *postLength = [NSString stringWithFormat:@"%lu", (long)[postData length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
    }
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    
    NSLog(@"CONNECTION: %@", conn);
    
    
}
- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse*)response {
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    
    int responseStatusCode = [httpResponse statusCode];
    
    NSLog(@"response = %d - %@", responseStatusCode, chamada);

    httpResponseStatus = responseStatusCode;
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    NSLog(@" --------- receive data !!! %@" , chamada);
    
    if (httpData == NULL) {
        httpData = [[NSMutableData alloc] init];
    }

    [httpData appendData:data];
        
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSLog(@"---     ----   deu :)  %@", chamada) ;
    
    if (httpResponseStatus == 200 || httpResponseStatus == 201 || httpResponseStatus == 204) {
            NSString *jsonString = [[NSString alloc] initWithData:httpData encoding:NSUTF8StringEncoding];
            httpData = NULL;
            NSLog(@"retorno %@",jsonString);
        
        if ([origem isEqualToString:@"fleury"]) {
            if ([listener respondsToSelector:selector]){
                [listener performSelector:selector withObject:jsonString];
            }
        }
        else{
            NSDictionary *jsonDict = [jsonString JSONValue];
            
            // testar se tem o selector antes de rodar...
            if ([listener respondsToSelector:selector]){
                [listener performSelector:selector withObject:jsonDict];
            }
        }
  
    } else {
        
        NSLog(@"RETORNO COM STATUS CODE = %d", httpResponseStatus);
        
        NSString *erroString = [[NSString alloc] initWithData:httpData encoding:NSUTF8StringEncoding];
        NSLog(@"retornou: %@", erroString);
        httpData = NULL;
        NSMutableDictionary *dicErro = [[NSMutableDictionary alloc] init];
        [dicErro setObject:[NSNumber numberWithInt:httpResponseStatus] forKey:@"http_status"];
        [dicErro setObject:erroString forKey:@"msg"];
        
        
        if ([listener respondsToSelector:failSelector]){
            NSLog(@"vai chamar o selector FAIL...");
            [listener performSelector:failSelector withObject:dicErro];
        }
        
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"ERRO DE CONEXÃO  --  %@ - %@", chamada, error);

    httpData = NULL;
    
    NSMutableDictionary *dicErro = [[NSMutableDictionary alloc] init];
    [dicErro setObject:chamada forKey:@"chamada"];
    
    if ([listener respondsToSelector:failSelector]){
        NSLog(@"vai chamar o selector FAIL...");
        [listener performSelector:failSelector withObject:dicErro];
    }

    
}


- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    NSLog(@"Chamou auth: %@", challenge.protectionSpace.host);
    
    NSString *configPath = [[NSBundle mainBundle] pathForResource:@"config" ofType:@"plist"];
    NSDictionary *dicConfig = [NSDictionary dictionaryWithContentsOfFile:configPath];
    NSArray *trustedHosts=[NSArray arrayWithObject:[dicConfig objectForKey:@"endServer"]];
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        if ([trustedHosts containsObject:challenge.protectionSpace.host])
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];

    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

@end
