//
//  ServerDelegate.h
//
//  Created by Douglas Amengual Severo on 06/06/13.
//  Copyright (c) 2013 Sevdroid Ltda. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ServerDelegate : NSObject {
    
    id listener;
    SEL selector;
    SEL failSelector;
    
    NSMutableData *httpData;
    int httpResponseStatus;
    
    Boolean isRetornoData;
    
    NSString *chamada;
    NSString *origem;
    
}


-(void)jsonPost: (id) sender withParam:(NSDictionary *) param andURL:(NSString *) url andOKSelector: (SEL) _selector andFAILSelector: (SEL) _failSelector;

-(void)getJson:(id)sender withParam:(NSString *) param andOKSelector:(SEL)_selector andFAILSelector:(SEL)_failSelector;

@end
